This tool produces XML resume including XSLT for nice look and XSD for data validation.
Just run resume_creator.py and you will see nice PyQt5 user interface.
You can input data, load existing xml, save your xml (with style files), open it in browser and generate pdf version of your resume. There are some examples in "example" dir.

File like style*.css, style*.xsl and output.xsd are static and they are not changed during generation.

This version has probably many bugs - I covered most of encountered errors but if you notice one - please let me know.

You can try to build it into a binary (I succeded with pyinstaller) but please remember that the executable needs to be inside a directory which parent dir is the main dir.

REQUIREMENTS:
Python 3 - best to use anaconda python distribution for your platform (https://anaconda.org/anaconda/python)
wkhtmltopdf for PDF generation (https://wkhtmltopdf.org/)
Requirements for Python 3 libs are in requirements.txt - just run "pip install -r requirements.txt"

