from xml_creator import XmlResumeCreator
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QWidget, QDialog, QMessageBox
from mainwindow import Ui_MainWindow
from job import Ui_Dialog as JobDialog
from school import Ui_Dialog as SchoolDialog
from skill import Ui_Dialog as SkillDialog
from interest import Ui_Dialog as InterestDialog
from shutil import copy
import pdfkit
import webbrowser
import os
import sys

class JobPopup(QDialog, JobDialog):

    def __init__(self, parent=None):
        super(self.__class__,self).__init__(parent)
        self.main_window = parent
        self.setupUi(self)
        self.accepted.connect(self.__add_job)

    def __add_job(self):
        company = self.company.text()
        position = self.position.text()
        duration = self.duration.text()
        description = self.description.toPlainText()
        self.main_window.resume_dict['experience'].append([company,position,duration,description])
        self.main_window.job_list.addItem(company.strip() + '\n' + position + '\n--------')


class SchoolPopup(QDialog, SchoolDialog):

    def __init__(self, parent=None):
        super(self.__class__,self).__init__(parent)
        self.main_window = parent
        self.setupUi(self)
        self.accepted.connect(self.__add_school)

    def __add_school(self):
        school = self.name.text()
        duration = self.duration.text()
        type = self.type.text()
        description = self.desc.toPlainText()
        self.main_window.resume_dict['schools'].append([school,type,duration,description])
        self.main_window.school_list.addItem(school.strip() + '\n--------')

class SkillPopup(QDialog, SkillDialog):

    def __init__(self, parent=None):
        super(self.__class__,self).__init__(parent)
        self.main_window = parent
        self.setupUi(self)
        self.accepted.connect(self.__add_skill)
        self.type.setCurrentItem(self.type.item(0))
        self.type.item(0).setSelected(True)

    def __add_skill(self):

        skill = self.name.text()
        typ = str(self.type.currentItem().text()).lower()
        if typ == 'language':
            description = self.level.text()
            self.main_window.resume_dict['skills'].append([skill,typ, description])
        else:
            self.main_window.resume_dict['skills'].append([skill,typ])
        print(self.main_window.resume_dict['skills'])
        self.main_window.skills.addItem(skill)

class InterestPopup(QDialog, InterestDialog):

    def __init__(self, parent=None):
        super(self.__class__,self).__init__(parent)
        self.main_window = parent
        self.setupUi(self)
        self.accepted.connect(self.__add_interest)

    def __add_interest(self):
        interest = self.name.text()
        self.main_window.interests.addItem(interest)



class ResumeCreatorApp(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(self.__class__,self).__init__()
        self.setupUi(self)
        self.__path = '../output'
        self.__xml_path = '../xml'
        self.__f_name = 'resume.xml'
        self.__current_style = ""
        self.__xsd_file = 'output.xsd'
        self.__styles = {}
        self.xml = XmlResumeCreator(os.path.join(self.__path,self.__f_name), os.path.join(self.__xml_path,self.__xsd_file))
        self.resume_dict = None
        self.__options_pdf = {
        'page-size': 'A4',
        'margin-top': '0.2in',
        'margin-right': '0.2in',
        'margin-bottom': '0.2in',
        'margin-left': '0.2in',
        'encoding': "UTF-8",
        'no-outline': None
        }
        self.__reset_resume()
        self.load.clicked.connect(self.__load_xml)
        self.clear.clicked.connect(self.__clear)
        self.job_list.itemClicked.connect(self.__job_clicked)
        self.school_list.itemClicked.connect(self.__school_clicked)
        self.skills.itemClicked.connect(self.__skill_clicked)
        self.add_job.clicked.connect(self.__add_job)
        self.remove_job.clicked.connect(self.__remove_job)
        self.add_school.clicked.connect(self.__add_school)
        self.remove_school.clicked.connect(self.__remove_school)
        self.add_skill.clicked.connect(self.__add_skill)
        self.remove_skill.clicked.connect(self.__remove_skill)
        self.add_interest.clicked.connect(self.__add_interest)
        self.remove_interest.clicked.connect(self.__remove_interest)
        self.save.clicked.connect(self.createResume)
        self.style.clicked.connect(self.changeStyle)
        self.open.clicked.connect(self.__open_browser)
        self.view.clicked.connect(self.__preview)
        self.export_2.clicked.connect(self.__to_pdf)
        self.fill_styles()

    def __to_pdf(self):
        try:
            pdfkit.from_file(os.path.join(self.__path,self.__f_name), os.path.join(self.__path,'resume.pdf'),
                             options=self.__options_pdf)
            self.msg = QMessageBox()
            self.msg.setIcon(QMessageBox.Information)
            self.msg.setText("Resume saved in pdf")
            self.msg.setInformativeText("Resume was saved in %s" % self.__path)
            self.msg.show()
        except OSError as error:
            self.msg = QMessageBox()
            self.msg.setIcon(QMessageBox.Critical)
            self.msg.setText("Requiremenets not met")
            self.msg.setInformativeText(
                "Make sure that you have wkhtmltopdf installed to use pdf export option\n\n%s" % str(error))
            self.msg.setWindowTitle("Error")
            self.msg.show()


    def __preview(self):
        with open(os.path.join(self.__path,self.__f_name)) as file:
            self.textBrowser.setText(file.read())

    def __open_browser(self):
        new = 2
        url = os.path.join(self.__path,self.__f_name)
        webbrowser.open(url, new=new)

    def fill_styles(self):
        self.__get_styles()
        for item in self.__styles:
            self.style.addItem(item)
        self.style.setCurrentItem(self.style.item(0))
        self.__current_style = self.style.item(0).text()

    def changeStyle(self,item):
        it = self.style.itemFromIndex(item)
        self.__current_style = it.text()

    def __get_styles(self):
        for root,dir,files in os.walk(self.__xml_path):
            for file in files:
                if str(file).endswith('.xsl'):
                    css = "".join(str(file).split('.')[:-1])
                    css += '.css'
                    if os.path.isfile(os.path.join(self.__xml_path,css)):
                        self.__styles[file] = css
        print(self.__styles)



    def createResume(self):
        self.xml.reset()
        self.xml.add_personal_info(self.f_name.text(), self.l_name.text())
        self.xml.add_address(self.city.text(), self.zip_code.text(),
                            self.street.text(), self.mobile.text(), self.email.text())
        self.xml.add_goal(self.goal.toPlainText())
        for position in self.resume_dict['experience']:
            self.xml.add_experience_position(position[0], position[1], position[2], position[3])
        for school in self.resume_dict['schools']:
            self.xml.add_education_position(school[0],school[1],school[2],school[3])
        for skill in self.resume_dict['skills']:
            if len(skill) == 3:
                self.xml.add_skill(skill[1],skill[0],skill[2])
            else:
                self.xml.add_skill(skill[1],skill[0])
        for index in range(self.interests.count()):
            self.xml.add_interest(self.interests.item(index).text())
        try:
            self.xml.write_to_xml()
        except AttributeError:
            self.msg = QMessageBox()
            self.msg.setIcon(QMessageBox.Critical)
            self.msg.setText("Error")
            self.msg.setInformativeText("XML couldn't be validated - be sure to fill all fields")
            self.msg.setWindowTitle("Error")
            self.msg.show()
            return
        try:
            self.__copy_xsd_xslt()
        except FileNotFoundError as e:
            self.msg = QMessageBox()
            self.msg.setIcon(QMessageBox.Critical)
            self.msg.setText("Error")
            self.msg.setInformativeText("Not all required files were found %s" % str(e))
            self.msg.setWindowTitle("Error")
            self.msg.show()
            return
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Information)
        self.msg.setText("Resume saved")
        self.msg.setInformativeText("Resume was saved in %s"% os.path.join(self.__path,self.__f_name))
        self.msg.show()

    def __copy_xsd_xslt(self):
        copy('../xml/output.xsd',os.path.join(self.__path,'output.xsd'))
        print(self.__current_style,self.__styles[self.__current_style])
        copy(os.path.join(self.__xml_path,self.__current_style),os.path.join(self.__path,'style.xsl'))
        copy(os.path.join(self.__xml_path,self.__styles[self.__current_style]),
             os.path.join(self.__path,self.__styles[self.__current_style]))

    def __add_interest(self):
        pop = InterestPopup(self)
        pop.show()

    def __remove_interest(self):
        self.interests.takeItem(self.interests.currentRow())

    def __add_skill(self):
        pop = SkillPopup(self)
        pop.show()

    def __remove_skill(self):
        i = self.skills.currentRow()
        self.resume_dict['skills'].pop(i)
        self.skills.takeItem(i)

    def __add_school(self):
        pop = SchoolPopup(self)
        pop.show()

    def __remove_school(self):
        i = self.school_list.currentRow()
        self.school_list.takeItem(i)
        self.resume_dict['schools'].pop(i)

    def __add_job(self):
        pop = JobPopup(self)
        pop.show()

    def __remove_job(self):
        i = self.job_list.currentRow()
        self.job_list.takeItem(i)
        self.resume_dict['experience'].pop(i)

    def __job_clicked(self, item):
        i = self.job_list.row(item)
        self.textBrowser.setText('\n'.join(str(p) for p in self.resume_dict['experience'][i]))

    def __school_clicked(self, item):
        i = self.school_list.row(item)
        self.textBrowser.setText('\n'.join(str(p) for p in self.resume_dict['schools'][i]))

    def __skill_clicked(self, item):
        i = self.skills.row(item)
        self.textBrowser.setText('\n'.join(str(p) for p in self.resume_dict['skills'][i]))

    def __reset_resume(self):
        self.resume_dict = {'experience':[],'schools':[],'skills':[]}

    def __clear(self):
        self.interests.clear()
        self.skills.clear()
        self.school_list.clear()
        self.job_list.clear()
        self.goal.clear()
        self.f_name.clear()
        self.l_name.clear()
        self.mobile.clear()
        self.email.clear()
        self.zip_code.clear()
        self.city.clear()
        self.street.clear()
        self.__reset_resume()

    def __load_xml(self):
        filename = QFileDialog().getOpenFileName()
        if filename[0]:
            self.xml.load_xml(filename[0])
            self.__clear()
            self.__fill_all()

    def __fill_all(self):
        self.__fill_personal()
        self.__fill_address()
        self.__fill_goal()
        self.__fill_experience()
        self.__fill_school()
        self.__fill_skills()
        self.__fill_interests()

    def __fill_personal(self):
        f_name, l_name = self.xml.get_personal_info()
        self.f_name.setText(f_name)
        self.l_name.setText(l_name)

    def __fill_address(self):
        city,p_code,str,phone,mail = self.xml.get_address()
        self.city.setText(city)
        self.zip_code.setText(p_code)
        self.street.setText(str)
        self.mobile.setText(phone)
        self.email.setText(mail)

    def __fill_experience(self):
        exp_list = self.xml.get_experience()
        self.resume_dict['experience'] = exp_list
        for exp in exp_list:
            self.job_list.addItem(exp[0].strip() + '\n' + exp[1].strip() + '\n--------')

    def __fill_goal(self):
        self.goal.setText(self.xml.get_goal())

    def __fill_school(self):
        sch_list = self.xml.get_education()
        self.resume_dict['schools'] = sch_list
        for school in sch_list:
            self.school_list.addItem(school[0].strip() + '\n--------')

    def __fill_skills(self):
        skills = self.xml.get_skills()
        self.resume_dict['skills'] = skills
        for skill in skills:
            self.skills.addItem(skill[0].strip())

    def __fill_interests(self):
        interests = self.xml.get_interests()
        for interest in interests:
            self.interests.addItem(interest.strip())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    resume_creator = ResumeCreatorApp()
    resume_creator.show()
    sys.exit(app.exec_())