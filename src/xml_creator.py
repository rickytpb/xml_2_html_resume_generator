import lxml.etree as ET
from xml.dom.minidom import parseString
import sys
import os

class XmlResumeCreator(object):
    def __init__(self, xml_name, xsd_path):
        self.filename = xml_name
        self.__xsd = xsd_path
        self.__root = self.__create_root(self.__xsd.split('/')[-1])
        self.__create_template()

    @staticmethod
    def prettify_and_validate(elem, xsd):
        prefix = '<?xml-stylesheet type="text/xsl" href="style.xsl"?>'
        rough_string = ET.tostring(elem, doctype=prefix)
        with open(xsd, 'r') as file:
            xmlschema = ET.XMLSchema(ET.XML(file.read()))
            if not xmlschema.validate(ET.XML(rough_string)):
                raise AttributeError
        reparsed = parseString(rough_string)
        return reparsed.toprettyxml(indent="\t")

    def reset(self):
        self.__root = None
        self.__root = self.__create_root(self.__xsd.split('/')[-1])
        self.__create_template()

    def __create_root(self, xsd_file):
        ns = "http://www.w3.org/2001/XMLSchema-instance"
        nsmap = {'xsi': ns}
        location_attribute = '{%s}noNamespaceSchemaLocation' % ns
        attrib = {location_attribute : xsd_file}
        return ET.Element('resume',attrib=attrib,nsmap=nsmap)

    def __create_template(self):
        self.__personal = ET.SubElement(self.__root, 'personal')
        self.__goal = ET.SubElement(self.__personal, 'career_goal')
        self.__info = ET.SubElement(self.__personal, 'info')
        self.__address = ET.SubElement(self.__personal, 'address')
        self.__experience = ET.SubElement(self.__root, 'experience')
        self.__education = ET.SubElement(self.__root, 'education')
        self.__additional_info = ET.SubElement(self.__root, 'additional_info')
        self.__skills = ET.SubElement(self.__additional_info, 'skills')
        self.__interests = ET.SubElement(self.__additional_info, 'interests')
        #self.s = ET.SubElement(self.__additional_info, 's')

    def add_personal_info(self, first, last):
        first_name = ET.SubElement(self.__info, 'first_name')
        first_name.text = first
        last_name = ET.SubElement(self.__info, 'last_name')
        last_name.text = last

    def get_personal_info(self):
        return self.__root.findtext(".//first_name"),self.__root.findtext(".//last_name")

    def add_address(self, city_t, postal_code_t, street_t, phone_t, mail):
        city = ET.SubElement(self.__address, 'city')
        city.text = city_t
        postal_code = ET.SubElement(self.__address, 'postal_code')
        postal_code.text = postal_code_t
        street = ET.SubElement(self.__address, 'street')
        street.text = street_t
        phone = ET.SubElement(self.__address, 'phone')
        phone.text = phone_t
        email = ET.SubElement(self.__address, 'e_mail')
        email.text = mail

    def get_address(self):
        return self.__root.findtext(".//city"), self.__root.findtext(".//postal_code"),\
               self.__root.findtext(".//street"), self.__root.findtext(".//phone"),\
               self.__root.findtext(".//e_mail")


    def add_goal(self, goal_text):
        self.__goal.text = goal_text

    def get_goal(self):
        return self.__root.findtext(".//career_goal")

    def add_experience_position(self, company, position, time, description):
        exp = ET.SubElement(self.__experience, 'company')
        exp.text = company
        pos = ET.SubElement(exp, 'position')
        pos.text = position
        duration = ET.SubElement(exp, 'duration')
        duration.text = time
        desc = ET.SubElement(exp, 'description')
        desc.text = description

    def get_experience(self):
        jobs = self.__root.findall('.//company')
        joblist = []
        for job in jobs:
            temp_job = []
            name = job.text.strip()
            position = job.findtext('./position').strip()
            duration = job.findtext('./duration').strip()
            description = job.findtext('./description').strip()
            temp_job.append(name)
            temp_job.append(position)
            temp_job.append(duration)
            temp_job.append(description)
            joblist.append(temp_job)
        return joblist


    def add_education_position(self, school, type, time, description):
        sch = ET.SubElement(self.__education, 'school')
        sch.text = school
        sch.set('type', str(type).lower())
        duration = ET.SubElement(sch, 'duration')
        duration.text = time
        desc = ET.SubElement(sch, 'description')
        desc.text = description

    def get_education(self):
        schools = self.__root.findall('.//school')
        schlist = []
        for school in schools:
            temp_sch = []
            name = school.text.strip()
            duration = school.findtext('./duration').strip()
            type = school.attrib['type']
            description = school.findtext('./description').strip()
            temp_sch.append(name)
            temp_sch.append(type)
            temp_sch.append(duration)
            temp_sch.append(description)
            schlist.append(temp_sch)
        return schlist

    def add_skill(self, type, skill_name, description=None):
        if type not in ['soft', 'technical', 'language']:
            raise AttributeError
        skill = ET.SubElement(self.__skills, 'skill')
        skill.text = skill_name
        skill.set('type', type)
        if description:
            desc = ET.SubElement(skill, 'description')
            desc.text = description

    def get_skills(self):
        skills = self.__root.findall('.//skill')
        sklist = []
        for skill in skills:
            temp_sk = []
            name = skill.text.strip()
            type = skill.get('type')
            try:
                description = skill.findtext('./description').strip()
            except AttributeError:
                description = None
            temp_sk.append(name)
            temp_sk.append(type)
            if description:
                temp_sk.append(description)
            sklist.append(temp_sk)
        return sklist

    def add_interest(self, type):
        interest = ET.SubElement(self.__interests, 'interest')
        interest.text = type

    def get_interests(self):
        interests = self.__root.findall('.//interest')
        inter_list = []
        for inter in interests:
            inter_list.append(inter.text.strip())
        return inter_list


    def write_to_xml(self):
        xml_data = self.prettify_and_validate(self.__root,self.__xsd)
        if not os.path.exists('../output'):
            os.mkdir('../output')
        with open(self.filename, 'w+') as xml_file:
            xml_file.write(xml_data)

    def load_xml(self, filename):
        self.__root = ET.parse(filename)


if __name__ == "__main__":
    x_creator = XmlResumeCreator('xml/output.xml','xml/output.xsd')
    x_creator.add_personal_info('Jacek', 'Smoliński')
    x_creator.add_address('Milanowek', '05-822', 'Czechowa 4', '555620635', 'bitwbit@gmail.com')
    x_creator.add_goal(
        'I want to excel the limits of QA engineering and automation. Looking for new ways to analyze the results and test software.')
    x_creator.add_experience_position('Vewd', 'QA Engineer', '09.2017 - present',
                                      'Developing and maintaining automated tests in Python. Working on new solutions .\
                                       Analyzing the results with the tools provided by statistical analysis. Making QA great again.')
    x_creator.add_experience_position('GOG.COM', 'Lead QA Engineer', '03.2016 - 08.2017',
                                      'Managing QA Engineers team to achieve best possible quality of the products. Bringing good vibe to the team and the company.\
                                       Developing automated tests in C++ and Python. Finding new ways to test software and analyze the results.')
    x_creator.add_education_position('Wyzsza Szkola Informatyki Stosowanej i Zarzadzania', 'bachelor', '2015 - present',
                                     'Bachelor studies')
    x_creator.add_skill('soft', 'Leader')
    x_creator.add_skill('soft', 'Hard worker')
    x_creator.add_skill('soft', 'Can do attitude')
    x_creator.add_skill('soft', 'Quick Learner')
    x_creator.add_skill('technical', 'C++')
    x_creator.add_skill('technical', 'Python')
    x_creator.add_skill('technical', 'Selenium')
    x_creator.add_skill('technical', 'Automated Testing')
    x_creator.add_skill('technical', 'Statistical Analysis')
    x_creator.add_skill('language', 'English', 'Fluent')
    x_creator.add_skill('language', 'Spanish', 'Basic')
    x_creator.add_skill('language', 'German', 'Basic')
    x_creator.add_interest('South American movies')
    x_creator.add_interest('Music production')
    x_creator.add_interest('Retro gaming')
    x_creator.write_to_xml()
