<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/resume">
    <html>
<head>
<title>Resume</title>

<meta name="viewport" content="width=device-width"/>
<meta name="description" content="Resume"/>
<meta charset="UTF-8"/>

<link type="text/css" rel="stylesheet" href="style.css"/>
<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Lato:400,300' rel='stylesheet' type='text/css'/>

</head>
<body id="top">
<div id="cv" class="instaFade">
	<div class="mainDetails">
		<div id="name">
			<h1 xml:space="preserve" class="quickFade delayTwo">
                <xsl:value-of select="personal/info/first_name"/>
				<div id="last_name">
                <xsl:value-of select="personal/info/last_name"/>
				</div>
            </h1>
		</div>

		<div id="contactDetails" class="quickFade delayFour">
			<ul>
				<li>e-mail: <a href="mailto:{/resume/personal/address/e_mail}" target="_blank">
                <xsl:value-of select='personal/address/e_mail'/>
                </a></li>
				<li>mobile: <xsl:value-of select='personal/address/phone'/>
                </li>
				<li xml:space="preserve">
                    <xsl:value-of select='personal/address/postal_code'/>
                    <xsl:value-of select='personal/address/city'/>
                    <br/>
                    <xsl:value-of select='personal/address/street'/>
                </li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>

	<div id="mainArea" class="quickFade delayFive">
		<section>
			<article>
				<div class="sectionTitle">
					<h1>Personal Profile</h1>
				</div>

				<div class="sectionContent">
					<p><xsl:value-of select="personal/career_goal"/></p>
				</div>
			</article>
			<div class="clear"></div>
		</section>


		<section>
			<div class="sectionTitle">
				<h1>Work Experience</h1>
			</div>

			<div class="sectionContent">
                <xsl:for-each select="experience/company">
				<article>
					<h2><xsl:value-of select="text()"/></h2>
                    <h3><xsl:value-of select="position"/></h3>
					<p class="subDetails"><xsl:value-of select="duration"/></p>
					<p><xsl:value-of select="description"/></p>
				</article>
                </xsl:for-each>
			</div>
			<div class="clear"></div>
		</section>

		<section>
			<div class="sectionTitle">
				<h1>Education</h1>
			</div>

			<div class="sectionContent">
                <xsl:for-each select="education">
				<article>
					<h2><xsl:value-of select="school/text()"/></h2>
					<p class="subDetails"><xsl:value-of select="school/duration"/></p>
					<p><xsl:value-of select="school/description"/></p>
				</article>
                </xsl:for-each>
			</div>
			<div class="clear"></div>
		</section>


		<section>
			<div class="sectionTitle">
				<h1>Soft skills</h1>
			</div>

			<div class="sectionContent">
				<ul class="keySkills">
                    <xsl:for-each select="additional_info/skills/skill[@type='soft']">
                        <li><xsl:value-of select="text()"/></li>
                    </xsl:for-each>
                </ul>
			</div>
			<div class="clear"></div>
        </section>

        <section>
			<div class="sectionTitle">
				<h1>Technical skills</h1>
			</div>

			<div class="sectionContent">
				<ul class="keySkills">
                    <xsl:for-each select="additional_info/skills/skill[@type='technical']">
                        <li><xsl:value-of select="text()"/></li>
                    </xsl:for-each>
                </ul>
			</div>
			<div class="clear"></div>
        </section>

        <section>
			<div class="sectionTitle">
				<h1>Languages</h1>
			</div>

			<div class="sectionContent">
				<ul class="keySkills">
                    <xsl:for-each select="additional_info/skills/skill[@type='language']">
                        <li><xsl:value-of select="text()"/>- <xsl:value-of select="description"/></li>
                    </xsl:for-each>
                </ul>
			</div>
			<div class="clear"></div>
        </section>

		<section>
			<div class="sectionTitle">
				<h1>Interests</h1>
			</div>

			<div class="sectionContent">
				<ul class="keySkills">
                    <xsl:for-each select="additional_info/interests/interest">
                        <li><xsl:value-of select="text()"/><xsl:value-of select="description"/></li>
                    </xsl:for-each>
                </ul>
			</div>
			<div class="clear"></div>
        </section>

	</div>
</div>
</body>
</html>

</xsl:template>

</xsl:stylesheet>